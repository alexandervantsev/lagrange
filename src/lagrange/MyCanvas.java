package lagrange;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import javax.swing.*;
public class MyCanvas extends JComponent {
    private int funcIndex = -1 ;
    private int argsIndex;
    private int width = 500, height = 500, offset = 20;
    private int xCoordOfYAxis, yCoordOfXAxis;
    private double scaleX, scaleY;
    MyCanvas() {
        setLayout(null);
        setSize(width, height);
        setVisible(true);
    }

    public void paint(Graphics my_picture) {
        paintBackground(my_picture);
        if (funcIndex != -1) {
            paintAxes(my_picture);
            paintBasicFunction(my_picture);
            paintLagrange(my_picture);
        }
    }

    private void paintBackground(Graphics my_picture){
        my_picture.setColor(Color.WHITE);
        my_picture.fillRect(0, 0, 500, 500);
    }

    private void paintAxes(Graphics my_picture){
        int argsCount = Data.getX().get(argsIndex).size();
        double minX = Data.getX().get(argsIndex).get(0),
                maxX = Data.getX().get(argsIndex).get(argsCount - 1),
                minY = Collections.min(Data.getY().get(funcIndex)),
                maxY = Collections.max(Data.getY().get(funcIndex));
        scaleX = (width - offset) / (maxX - minX);
        scaleY = (height - offset) / (maxY - minY);
        int firstCutX, firstCutY;
        my_picture.setColor(Color.BLACK);
        if (minX >= 0){
            firstCutX = 30;
            xCoordOfYAxis = offset / 2;
        } else if (maxX <= 0){
            firstCutX = - 30;
            xCoordOfYAxis = width - offset / 2;
        } else {
            firstCutX = (maxX*scaleX > 30)? 30: -30;
            xCoordOfYAxis = width - offset - (int) (maxX * scaleX);
        }
        if (minY >= 0){
            firstCutY = - 30;
            yCoordOfXAxis = height - offset / 2;
        } else if (maxY <= 0){
            firstCutY = 30;
            yCoordOfXAxis = offset / 2;
        } else {
            firstCutY = (maxX*scaleX > 30)? - 30: 30;
            yCoordOfXAxis = offset + (int) (maxY * scaleY);
        }
        paintYAxis(my_picture, firstCutY, scaleY);
        paintXAxis(my_picture, firstCutX, scaleX);
    }



    private void paintBasicFunction(Graphics my_picture){
        int argsCount = Data.getX().get(argsIndex).size();
        double start = Data.getX().get(argsIndex).get(0);
        double end = Data.getX().get(argsIndex).get(argsCount - 1);
        Function<Double,Double> func = (funcIndex % 2 == 0) ? Math::sin : x -> 2*Math.cos(x) + x;
        my_picture.setColor(Color.RED);
        double absLength = end - start;
        List<Double> y = new ArrayList<>();
        double step = absLength * 1.1 / 100;
        double currentX = start - step;
        for (int i = 0; i < 100; i++){
            y.add(-func.apply(currentX)*scaleY  + yCoordOfXAxis );
            currentX += step;
        }
        currentX = (start-step)*scaleX + xCoordOfYAxis;
        step = absLength*1.1/100*scaleX;
        for (int i = 1; i < 100; i++){
            my_picture.drawLine((int)currentX,(int)(0 + y.get(i-1)),(int)(currentX+step),(int)(0 + y.get(i)));
            currentX += step;
        }
    }

    private void paintLagrange(Graphics my_picture){
        int argsCount = Data.getX().get(argsIndex).size();
        double start = Data.getX().get(argsIndex).get(0);
        double end = Data.getX().get(argsIndex).get(argsCount - 1);
        Function<Double,Double> func = (funcIndex % 2 == 0) ? Math::sin : x -> 2*Math.cos(x) + x;
        my_picture.setColor(Color.BLUE);
        double absLength = end - start;
        List<Double> y = new ArrayList<>();
        double step = absLength * 1.1 / 100;
        double currentX = start - step;
        for (int i = 0; i < 100; i++){
            y.add(-Lagrange.lagrange(Data.getY().get(funcIndex), Data.getX().get(argsIndex), currentX)*scaleY  + yCoordOfXAxis );
            currentX += step;
        }
        currentX = (start-step)*scaleX + xCoordOfYAxis;
        step = absLength * 1.1 / 100*scaleX;
        for (int i = 1; i < 100; i++){
            my_picture.drawLine((int)currentX,(int)(0 + y.get(i-1)),(int)(currentX+step),(int)(0 + y.get(i)));
            currentX += step;
        }
        for (int i = 0; i < Data.getX().get(argsIndex).size(); i++){
            double x = Data.getX().get(argsIndex).get(i);
            int posX =  (int) (x*scaleX) + xCoordOfYAxis ;
            int posY = (int) (-func.apply(x)*scaleY)  + yCoordOfXAxis;
            my_picture.fillOval(posX - 2, posY-2, 4, 4);
        }
    }


    private void paintYAxis(Graphics my_picture, int firstCut, double scale){
        my_picture.drawLine(xCoordOfYAxis, offset/2, xCoordOfYAxis, height - offset/2);
        my_picture.drawLine(xCoordOfYAxis, offset/2, xCoordOfYAxis - 6, offset/2 + 6);
        my_picture.drawLine(xCoordOfYAxis, offset/2, xCoordOfYAxis + 6, offset/2 + 6);
        my_picture.drawString("y",xCoordOfYAxis + 8,offset/2);
        my_picture.drawLine(xCoordOfYAxis - 4, yCoordOfXAxis + firstCut, xCoordOfYAxis +4, yCoordOfXAxis + firstCut);
        String scaleY = String.valueOf(-firstCut/scale);
        scaleY = scaleY.substring(0, scaleY.indexOf('.')+3);
        my_picture.drawString(scaleY, xCoordOfYAxis + 10,yCoordOfXAxis + firstCut);
    }
    private void paintXAxis(Graphics my_picture, int firstCut, double scale){
        my_picture.drawLine(offset/2, yCoordOfXAxis, width - offset/2, yCoordOfXAxis);
        my_picture.drawLine(width - offset/2, yCoordOfXAxis, width - offset/2 - 6, yCoordOfXAxis - 6);
        my_picture.drawLine(width - offset/2, yCoordOfXAxis, width - offset/2 - 6, yCoordOfXAxis + 6);
        my_picture.drawString("x",width - offset/2 - 6,yCoordOfXAxis - 10);
        my_picture.drawLine(firstCut + xCoordOfYAxis, yCoordOfXAxis - 4, firstCut + xCoordOfYAxis, yCoordOfXAxis +4);
        String scaleX = String.valueOf(firstCut/scale);
        scaleX = scaleX.substring(0, scaleX.indexOf('.')+3);
        my_picture.drawString(scaleX, firstCut + xCoordOfYAxis,yCoordOfXAxis + 15);
    }

    void setArgs(int func, int args) {
        this.funcIndex = args*2 + func;
        this.argsIndex = args;
    }
}