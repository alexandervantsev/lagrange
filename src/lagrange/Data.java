package lagrange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
public class Data {
    private static ArrayList<ArrayList<Double>> x = new ArrayList<>();
    private static ArrayList<ArrayList<Double>> y = new ArrayList<>();

    public static void setX(ArrayList<ArrayList<Double>> x) {
        Data.x = x;
    }

    public static ArrayList<ArrayList<Double>> getX() {
        return x;
    }

    public static ArrayList<ArrayList<Double>> getY() {
        return y;
    }

    public static void setY(ArrayList<ArrayList<Double>> y) {
        Data.y = y;
    }
    static void addY(ArrayList<Double> newList) {
        Data.y.add(newList);
    }
    static void addX(ArrayList<Double> newList) {
        Data.x.add(newList);
    }
}
