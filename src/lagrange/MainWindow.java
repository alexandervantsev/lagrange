package lagrange;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

class MainWindow extends JFrame{

    MainWindow(){

        super("Lagrange");
        Dimension dim = new Dimension(740,640),
                dimComboBox = new Dimension(60,30),
                dimMessage = new Dimension(500,20);
        this.setMinimumSize(dim);
        this.setResizable(false);
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        MyCanvas myGraphics = new MyCanvas();

        JRadioButton func1RadioButton = new JRadioButton("sin(x)");
        JRadioButton func2RadioButton = new JRadioButton("2cos(x)+x");


        JButton okButton = new JButton("OK");
        JTextPane message = new JTextPane();
        message.setEditable(false);
        message.setMaximumSize(dimMessage);

        JTextField xTextField = new JTextField();
        JButton calcButton = new JButton("Calculate");
        JLabel answerLabel = new JLabel("Answer:");
        JTextField answerTextField = new JTextField();
        answerTextField.setEditable(false);

        xTextField.setMaximumSize(dimComboBox);
        answerTextField.setMaximumSize(dimComboBox);

        IOHandler ioHandler = new IOHandler();
        int resultCode = ioHandler.scanFile();

        switch(resultCode){
            case 0: message.setText("The file has been successfully read"); break;
            case 1: message.setText("An error occured while reading the file"); break;
        }
        String[] args = getArgs(Data.getX());
        JComboBox<String> argsComboBox = new JComboBox<>(args);
        argsComboBox.setMaximumSize(dimComboBox);
        argsComboBox.setSelectedIndex(-1);

        func1RadioButton.addActionListener(e -> {
            func2RadioButton.setSelected(false);
            int funcIndex = 0;
            int index = argsComboBox.getSelectedIndex();
            if (argsComboBox.getSelectedIndex() != -1){
                message.setText("x: " + Data.getX().get(index).toString() +"\n"+ "y: " +Data.getY().get(index*2 + funcIndex).toString());
            }
        });

        func2RadioButton.addActionListener(e -> {
            func1RadioButton.setSelected(false);
            int funcIndex = 1;
            int index = argsComboBox.getSelectedIndex();
            if (argsComboBox.getSelectedIndex() != -1){
                message.setText("x: " + Data.getX().get(index).toString() +"\n"+ "y: " +Data.getY().get(index*2 + funcIndex).toString());
            }
        });

        argsComboBox.addItemListener(e -> {
            if (argsComboBox.getSelectedIndex() == -1) return;
            if (!func1RadioButton.isSelected() & !func2RadioButton.isSelected()){
                message.setText("Choose the function");
                argsComboBox.setSelectedIndex(-1);
                return;
            }
            int index = argsComboBox.getSelectedIndex();
            int funcIndex = func1RadioButton.isSelected() ?  0 : 1;
            message.setText("x: " + Data.getX().get(index).toString() +"\n"+ "y: " +Data.getY().get(index*2 + funcIndex).toString());
        });

        okButton.addActionListener(e -> {
            if (!func1RadioButton.isSelected() & !func2RadioButton.isSelected()){
                message.setText("Choose the function");
                return;
            }
            if (argsComboBox.getSelectedIndex() == -1) {
                message.setText("Choose arguments");
                return;
            }
            int funcIndex = func1RadioButton.isSelected() ?  0 : 1;
            int argsIndex = argsComboBox.getSelectedIndex();
            myGraphics.setArgs(funcIndex, argsIndex);
            myGraphics.repaint();

        });

        calcButton.addActionListener(e -> {
            if (!func1RadioButton.isSelected() & !func2RadioButton.isSelected()){
                message.setText("Choose the function");
                return;
            }
            if (argsComboBox.getSelectedIndex() == -1) {
                message.setText("Choose arguments");
                return;
            }
            double x;
            try {
                x = Double.parseDouble(xTextField.getText());
            } catch (NullPointerException eq){
                message.setText("Enter x");
                return;
            } catch (NumberFormatException ee){
                message.setText("x must be a decimal number");
                return;
            }
            int funcIndex = func1RadioButton.isSelected() ?  0 : 1;
            int argsIndex = argsComboBox.getSelectedIndex();
            double resultDouble = Lagrange.lagrange(Data.getY().get(argsIndex*2+funcIndex), Data.getX().get(argsIndex), x);
            String result = String.valueOf(resultDouble);
            if (result.substring(result.indexOf('.')).length() > 4)
            result = result.substring(0,result.indexOf('.')+3);
            answerTextField.setText(result);
        });





        layout.setHorizontalGroup(layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(func1RadioButton)
                        .addComponent(func2RadioButton)
                        .addComponent(argsComboBox)
                        .addComponent(okButton)
                            .addGap(90)
                        .addComponent(xTextField)
                        .addComponent(calcButton)
                            .addComponent(answerLabel)
                            .addComponent(answerTextField))
                    .addComponent(myGraphics))
                .addComponent(message)
                );

        layout.setVerticalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addComponent(func1RadioButton)
                    .addComponent(func2RadioButton)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(argsComboBox))
                    .addComponent(okButton)
                        .addGap(90)
                        .addComponent(xTextField)
                        .addComponent(calcButton)
                        .addComponent(answerLabel)
                        .addComponent(answerTextField))
                .addComponent(myGraphics))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        .addComponent(message)));

        pack();
        setVisible(true);
    }

    private String[] getArgs(ArrayList<ArrayList<Double>> listList){
        int size = listList.size();
        String[] result = new String[size];
        for (int i = 0; i < size; i++){
            result[i] = "Args set №"+ (i+1);
        }
        return result;
    }

}
