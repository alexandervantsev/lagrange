package lagrange;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.List;

public class IOHandler {
    private File file = new File("C://Users/Floyd/Desktop/ИТМО/Computational Maths/lagrange/data.txt");

    /*public File getFile() {
        return file;
    }*/

    public int scanFile(){
        int resultCode = 0;
        try {
            Scanner fileScanner = new Scanner(file);
            while (fileScanner.hasNextLine()){
                ArrayList<Double> next;
                String nextLine = fileScanner.nextLine();
                Scanner lineScanner = new Scanner(nextLine);
                lineScanner.useLocale(Locale.US);
                next = getNextLineArgs(lineScanner);
                Data.addX(next);
                for (int i = 0; i < 2; i++){
                    nextLine = fileScanner.nextLine();
                    lineScanner = new Scanner(nextLine);
                    lineScanner.useLocale(Locale.US);
                    next = getNextLineArgs(lineScanner);
                    Data.addY(next);
                }
            }

        } catch (FileNotFoundException e){
            resultCode = 1;
        }
        return resultCode;
    }

    private ArrayList<Double> getNextLineArgs(Scanner scanner){
        ArrayList<Double> list = new ArrayList<>();
        double nextNumber = 0.0;
        while (scanner.hasNextDouble()){
            nextNumber = scanner.nextDouble();
            list.add(nextNumber);
        }
        return list;
    }

}
